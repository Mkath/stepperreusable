package com.kath.stepperreusable;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kath.stepperlibrary.StepperFragment;


/**
 * Created by Katherine on 7/16/2019
 */
public class LoaderFragment extends StepperFragment {
    @Override
    public boolean onNextButtonHandler() {
        return true;
    }

    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(
                R.layout.fragment_loader, container, false);
    }


}
