package com.kath.stepperreusable;

import android.content.DialogInterface;

import com.kath.stepperlibrary.MobileStepper;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends MobileStepper {

   List<Class> stepperFragmentList = new ArrayList<>();


    @Override
    public List<Class> init() {
        stepperFragmentList.add(FirstFragment.class);
        stepperFragmentList.add(SelectFragment.class);
        stepperFragmentList.add(LoaderFragment.class);
        stepperFragmentList.add(FirstFragment.class);
        stepperFragmentList.add(FirstFragment.class);
        stepperFragmentList.add(SelectFragment.class);
        stepperFragmentList.add(LoaderFragment.class);

        return stepperFragmentList;
    }


    @Override
    public void onStepperCompleted() {
        showCompletedDialog();
    }

    protected void showCompletedDialog(){
        android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(
                MainActivity.this);

        // set title
        alertDialogBuilder.setTitle("Bien hecho");
        alertDialogBuilder
                .setMessage("Hemos completado todos los pasos")
                .setCancelable(true)
                .setPositiveButton("Si",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {

                    }
                });

        // create alert dialog
        android.support.v7.app.AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();

    }
}
