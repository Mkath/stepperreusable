package com.kath.stepperlibrary;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Katherine on 7/16/2019
 */
public abstract class MobileStepper extends AppCompatActivity implements View.OnClickListener {

    private final String CURRENT = "CURRENT";
    private final String STEPPER_BASE = "STEPPER_BASE";

    private Button mPrevious;
    private TextView mStepText;
    List<Class> mStepperFragmentList;
    private BaseStepper mBaseStepper;
    private int recover_current_state = 0;
    private ScrollView mScroll;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_simple_stepper);

        Button mNext = (Button) findViewById(R.id.next);
        mPrevious = (Button)findViewById(R.id.back);
        mStepText = (TextView)findViewById(R.id.steps);
        ViewPager mViewPager = (ViewPager) findViewById(R.id.viewpager);
        mScroll = (ScrollView)findViewById(R.id.mobilescroll);

        mNext.setOnClickListener(this);
        mPrevious.setOnClickListener(this);
        if(savedInstanceState!=null) {
            if(savedInstanceState.getSerializable(STEPPER_BASE)!=null) {
                try {
                    mStepperFragmentList = (List<Class>) savedInstanceState.getSerializable(STEPPER_BASE);
                    recover_current_state = savedInstanceState.getInt(CURRENT);
                }catch(Exception e){
                    //it's  okay we will recover from the init method
                    mStepperFragmentList = init();
                }
            }
            else{
                mStepperFragmentList = init();
            }
        }
        else
        {
            mStepperFragmentList = init();
        }
        mBaseStepper = new BaseStepper(mViewPager, mStepperFragmentList, getSupportFragmentManager());
        mBaseStepper.CURRENT_PAGE = recover_current_state;
        recover_current_state = 0;
        BackButtonConfig();
        updateUI();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(STEPPER_BASE,(Serializable)mStepperFragmentList);
        outState.putInt(CURRENT, mBaseStepper.CURRENT_PAGE);
        super.onSaveInstanceState(outState);

    }

    protected void BackButtonConfig(){
        if(mBaseStepper.CURRENT_PAGE==0)
            mPrevious.setVisibility(View.INVISIBLE);
        else
            mPrevious.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.next) {
            if (checkStepper()) {
                mBaseStepper.onNextButtonClicked();
                BackButtonConfig();
                updateUI();
            }

        } else if (i == R.id.back) {
            mBaseStepper.onBackButtonClicked();
            BackButtonConfig();
            updateUI();

        }
    }
    public int getCurrentFragmentId(){
        return mBaseStepper.CURRENT_PAGE;
    }

    public boolean checkStepper(){
        if(mBaseStepper.resolveNavigation()){
            return true;
        }
        onStepperCompleted();
        return  false;

    }
    public void updateUI(){
        mStepText.setText("Step " + (mBaseStepper.CURRENT_PAGE + 1) + " of " + mBaseStepper.TOTAL_PAGE);
        mScroll.pageScroll(View.FOCUS_UP);
    }
    public abstract void onStepperCompleted();
    public abstract List<Class> init();

}
