package com.kath.stepperlibrary;

import android.support.v4.app.Fragment;

/**
 * Created by Katherine on 7/16/2019
 */
public abstract class StepperFragment extends Fragment {

    public abstract boolean onNextButtonHandler();

}
